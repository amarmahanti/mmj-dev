<?php

	$latty = $_REQUEST['latitude'];
	$longy = $_REQUEST['longitude'];
	$zoomy = $_REQUEST['zoom'];
	$catty = $_REQUEST['category'];
	
	// zoom calcs
	//if ($zoomy < 8) {$zoomy = 8;}
	//if ($zoomy > 16) {$zoomy = 16;}
	$dfc = 5000 / pow($zoomy, 4); // distance from center; varies based on current zoom

	// log in to our database
	include("config.php");

	// build the proper query (I took out the filter parameter while I was figuring this out, should work if we pass it)
	if ($filter) {
		$query = "select * from jmap_main where job_category = '".$catty."' && latitude between ".($latty - $dfc)." and ".($latty + $dfc)." && longitude between ".($longy - $dfc)." and ".($longy + $dfc)." && (t1.job_title like '%".$filter."%' || t1.extra like '%".$filter."%') ";
	} else {
		$query = "select * from jmap_main where job_category = '".$catty."' && latitude between ".($latty - $dfc)." and ".($latty + $dfc)." && longitude between ".($longy - $dfc)." and ".($longy + $dfc);
	}
	$results = mysql_query($query);
	$num_results = mysql_num_rows($results);

	$latlng = array();
	// set count
	$latlng[0]['job_count'] = $num_results;
	
	// build the markers
	$counter = 0;
	while ($row = mysql_fetch_assoc($results)) {
		$latlng[$counter]['id'] = $row['id'];
		$latlng[$counter]['lat'] = $row['latitude'];
		$latlng[$counter]['lng'] = $row['longitude'];
		if (preg_match("^federalgovernmentjobs^", $row['original_craigslist_url'])) {
			// build the extra stuff through jmap_main_extra
			$latlng[$counter]['description'] = $row['job_title'];
			$latlng[$counter]['source'] = "federalgovernmentjobs";
			$latlng[$counter]['extra'] = "<div class='descrip-label'>".$row['job_title']."</div>";
			$latlng[$counter]['extra'] .= "<div style='color:black'>".$row['extra']."</div>";
			$latlng[$counter]['extra'] .= "<div><a target='_blank' href='".$row['original_craigslist_url']."' class='infobox-link' target='_blank'><div style='font-weight:bold;text-decoration:underline'>Apply Here</div></a></div>";
		} elseif (preg_match("^snagajob^", $row['original_craigslist_url'])) {
			$latlng[$counter]['description'] = $row['job_title'];
			$latlng[$counter]['source'] = "snagajob";
			$latlng[$counter]['extra'] = "<div class='descrip-label'>".$row['job_title']."</div>";					
			$latlng[$counter]['extra'] .= "<div><a href='".$row['original_craigslist_url']."' class='infobox-link' target='_blank'><div style='font-weight:bold;text-decoration:underline'>Apply Here</div></a></div>";
		} elseif (preg_match("^www.recruiter.com^", $row['original_craigslist_url'])) {
			$latlng[$counter]['description'] = $row['job_title'];
			$latlng[$counter]['source'] = "recruiter";
			$latlng[$counter]['extra'] = "<div class='descrip-label'>".$row['job_title']."</div>";		
			$latlng[$counter]['extra'] .= "<div style='color:black'>".$row['extra']."</div>";
			$latlng[$counter]['extra'] .= "<div><a target='_blank' href='".$row['original_craigslist_url']."' class='infobox-link' target='_blank'><div style='font-weight:bold;text-decoration:underline'>Apply Here</div></a></div>";			
		} elseif (preg_match("^ziprecruiter^", $row['original_craigslist_url'])) {
			$latlng[$counter]['description'] = $row['job_title'];
			$latlng[$counter]['source'] = "ziprecruiter";
			$latlng[$counter]['extra'] = "<div class='descrip-label'>".$row['job_title']."</div>";		
			$latlng[$counter]['extra'] .= "<div style='color:black'>".$row['extra']."...</div>";		
			$latlng[$counter]['extra'] .= "<div><a href='".$row['original_craigslist_url']."' class='infobox-link' target='_blank'><div style='font-weight:bold;text-decoration:underline'>Apply Here</div></a></div>";
		} elseif (preg_match("^monster^", $row['original_craigslist_url'])) {
			$latlng[$counter]['extra'] = "<div class='descrip-label'>".$row['job_title']."</div>";
			$latlng[$counter]['extra'] .= "<div style='color:black'>".$row['extra']."</div>";
			$latlng[$counter]['description'] = $row['job_title'];
			$latlng[$counter]['source'] = "monster";
			$latlng[$counter]['extra'] .= "<div><a href='".$row['original_craigslist_url']."' class='infobox-link' target='_blank'><div style='font-weight:bold;text-decoration:underline'>Apply Here</div></a></div>";
		}  elseif (preg_match("^craigslist^", $row['original_craigslist_url'])) {
			$latlng[$counter]['extra'] = "<div class='descrip-label'>".$row['job_title']."</div>";
			$latlng[$counter]['extra'] .= "<div style='color:black'>".$row['extra']."</div>";
			$latlng[$counter]['description'] = $row['job_title'];
			$latlng[$counter]['source'] = "craigslist";
			$latlng[$counter]['extra'] .= "<div><a href='".$row['reply_to']."' class='infobox-link'>Apply Here</a></div>";
			$latlng[$counter]['extra'] .= "<div><a href='".$row['original_craigslist_url']."' class='infobox-link' target='_blank'><div style='font-weight:bold;text-decoration:underline'>Apply Here</div></a></div>";
		}  elseif (preg_match("^jobing^", $row['original_craigslist_url'])) {
			$latlng[$counter]['extra'] = "<div class='descrip-label'>".$row['job_title']."</div>";
			$latlng[$counter]['extra'] .= "<div style='color:black'>".$row['extra']."</div>";
			$latlng[$counter]['description'] = $row['job_title'];
			$latlng[$counter]['source'] = "jobing";
			//$latlng[$counter]['extra'] .= "<div><a href='mailto:".$row['reply_to']."' class='infobox-link'>Click Here to Reply via E-mail</a></div>";
			$lcase_state = strtolower($row['citystate']);
			$lcase_state = str_replace(" ","",$lcase_state);
			$urltopass = str_replace("www",$lcase_state,$row['original_craigslist_url']);
			$latlng[$counter]['extra'] .= "<div><a href='".$urltopass."' class='infobox-link' target='_blank'><div style='font-weight:bold;text-decoration:underline'>Apply Here</div></a></div>";
		}  elseif (preg_match("^career\.jobs\.net^", $row['original_craigslist_url'])) {
			$latlng[$counter]['extra'] = "<div class='descrip-label'>".$row['job_title']."</div>";
			$latlng[$counter]['extra'] .= "<div style='color:black'>".$row['extra']."</div>";
			$latlng[$counter]['description'] = $row['job_title'];
			$latlng[$counter]['source'] = "careerjobsnet";
			//$latlng[$counter]['extra'] .= "<div><a href='mailto:".$row['reply_to']."' class='infobox-link'>Click Here to Reply via E-mail</a></div>";
			$latlng[$counter]['extra'] .= "<div style='color:black'><a href='".$row['original_craigslist_url']."' class='infobox-link' target='_blank'><div style='font-weight:bold;text-decoration:underline'>Apply Here</div></a></div>";
		}   elseif (preg_match("^www\.jobster\.com^", $row['original_craigslist_url'])) {
			$latlng[$counter]['extra'] = "<div class='descrip-label'>".$row['job_title']."</div>";
			$latlng[$counter]['extra'] .= "<div style='color:black'>".$row['extra']."</div>";
			$latlng[$counter]['description'] = $row['job_title'];
			$latlng[$counter]['source'] = "jobsterdotcom";
			$latlng[$counter]['extra'] .= "<div style='color:black'><strong>Company :</strong><a href= 'http://www.jobster.com".$row['company_href']."' class='infobox-link' target='_blank'>".$row['company_name']."</a></div>";
			$latlng[$counter]['extra'] .= "<div style='color:black'><a href= '".$row['reply_to']."' class='infobox-link' target='_blank'>Apply Here</a></div>";
		}   elseif (preg_match("^www\.simplyhired\.com^", $row['original_craigslist_url'])) {
			$latlng[$counter]['extra'] = "<div class='descrip-label'>".$row['job_title']."</div>";
			$latlng[$counter]['extra'] .= "<div style='color:black'>".$row['extra']."</div>";
			$latlng[$counter]['description'] = $row['job_title'];
			$latlng[$counter]['source'] = "simplyhired";
			$latlng[$counter]['extra'] .= "<div style='color:black'><a href='".$row['original_craigslist_url']."/".$row['reply_to']."' class='infobox-link' target='_blank'><div style='font-weight:bold;text-decoration:underline'>Apply Here</div></a></div>";
		}   elseif (preg_match("^us\.jobs^", $row['original_craigslist_url'])) {
			$latlng[$counter]['extra'] = "<div class='descrip-label'>".$row['job_title']."</div>";
			$latlng[$counter]['description'] = $row['job_title'];
			$latlng[$counter]['source'] = "usjobs";
			$latlng[$counter]['extra'] .= "<div style='color:black'><a  target='_blank' href='".$row['reply_to']."' class='infobox-link'>Apply Here</a></div>";
		}   elseif (preg_match("^www\.careerbuilder\.com^", $row['original_craigslist_url'])) {
			$latlng[$counter]['extra'] = "<div class='descrip-label'>".$row['job_title']."</div>";
			$latlng[$counter]['extra'] .= "<div style='color:black'>".$row['extra']."</div>";
			$latlng[$counter]['description'] = $row['job_title'];
			$latlng[$counter]['source'] = "careerbuilder";
			$latlng[$counter]['extra'] .= "<div><a href='".$row['original_craigslist_url']."' class='infobox-link' target='_blank'><div style='font-weight:bold;text-decoration:underline'>Apply Here</div></a></div>";
		}    elseif (preg_match("^livecareer^", $row['original_craigslist_url'])) {
			$latlng[$counter]['extra'] = "<div class='descrip-label'>".$row['job_title']."</div>";
			$latlng[$counter]['extra'] .= "<div style='color:black'>".$row['extra']."</div>";
			$latlng[$counter]['description'] = $row['job_title'];
			$latlng[$counter]['source'] = "livecareer";
			$latlng[$counter]['extra'] .= "<div><a href='".$row['original_craigslist_url']."' class='infobox-link' target='_blank'><div style='font-weight:bold;text-decoration:underline'>Apply Here</div></a></div>";
		}   elseif (preg_match("^jobright^", $row['original_craigslist_url'])) {
			$latlng[$counter]['extra'] = "<div class='descrip-label'>".$row['job_title']."</div>";
			$latlng[$counter]['extra'] .= "<div style='color:black'>".$row['extra']."</div>";
			$latlng[$counter]['description'] = $row['job_title'];
			$latlng[$counter]['source'] = "jobright";
			$latlng[$counter]['extra'] .= "<div><a class='jobright_link' onclick='".addslashes($row['reply_to'])."' target='_blank'><div style='font-weight:bold;text-decoration:underline'>Apply Here</div></a></div>";
		}
		
		// build container
		
		// add the main container and the "x" button to close
		$close_string = "<div class = \'job-profile-item-content\'><div = onclick=\'jQuery(this).parent().fadeOut();jQuery(this).fadeOut();\' class=\'close-item-container\'><img style=\'width:20px;height:20px;\' src=\'/img/close.png\' /></div>";
		
		// set the Add to Profile link
		$X=addslashes($latlng[$counter]['extra']);
		$latlng[$counter]['extra'] .="<div class='button-add-to-profile' onclick=\"addToProfile('".$close_string.$X."')\">ADD TO LIST</div></div>";
		
		// attach the current center lat and lng to first result
		if ($counter == 0) {
			$latlng[0]['center_city_lat'] = $latty;
			$latlng[0]['center_city_long'] = $longy;
			$latlng[0]['dfc'] = $dfc;
		}
		
		// next marker
		$counter++;
	}

	// kicks out all markers within a certain rectangular area -- working
	print json_encode($latlng);

	// kicks inputs back out; for debugging purposes
	// $debug_info = array($latty, $longy, $zoomy, $catty);
	// print json_encode($debug_info);
	
?>