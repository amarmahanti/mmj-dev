// author : Jeet Mahanti
// date : September 21, 2016

// globals
var allMarkers = [];
var openBoxes = [];
var already_exists;
var map;
var prg;

function addToProfile(content) {
	jQuery("#right-container-element-top").append("<div style='color:#ffffff'>" + content + "</div>").fadeIn();
	return false;
}

function initialize(category, city, searchTerm) {
	// initial load progress bar
	jQuery("#progress").show();
	
    // initialize
	var json;
	jQuery.ajax({dataType:"json",url: "/ajax/getMarkers.php",data:{cat:category, city:city, filter:searchTerm}, success: function(result){
		var myLatlng = new google.maps.LatLng(result[0]['center_city_lat'], result[0]['center_city_long']);

		// select the correct options
		$("#cat-selector").val(category);
		$("#city-selector").val(city);

		// draw map
		var mapOptions = {
			zoom : 10,
			center : myLatlng,
			gestureHandling: "greedy"
		}
		map = new google.maps.Map(document.getElementById('google-maps-container'),mapOptions);
		populateMap(result);

		$("#progress").hide();

		// map event listners
		map.addListener('dragend', function() {
			reDraw(category);
		});

		map.addListener('zoom_changed', function() {
			reDraw(category);
		});		
	}});
}


function reDraw(lCat) {
	jQuery("#progress").fadeIn();
	
	// find the center latitude and longitude
	center = map.getCenter();
	var lLat = center.lat();
	var lLong = center.lng();
	var lZoom = map.getZoom();
//alert("zoom level is " + lZoom);
	// do ajax, get points
	jQuery.ajax({data:{"latitude":lLat, "longitude":lLong, "zoom":lZoom, "category":lCat}, method:"POST",url: "/ajax/drag_map_getMarkers.php", dataType: "json", success: function(result){
		// repopulate map
		populateMap(result);
	}});
	
}

function populateMap(result) {
	var listCenter = map.getCenter();
	var listLat = listCenter.lat();
	var listLong = listCenter.lng();
	var listZoom = map.getZoom();
	
	for (var i = 0, length = result.length; i < length; i++) {
		// set lat lng to city center if null cause of recruiter bailing out on geocoding not google bailing out
		// should never happen on reDraw but let's see
		if (!result[i].lat & !result[i].lng) {
			var latLng = new google.maps.LatLng(result[0]['center_city_lat'],result[0]['center_city_long']);
			} else {
			var latLng = new google.maps.LatLng(result[i].lat,result[i].lng);
		}
		var markerTitle = result[i].description;
		// assign marker a unique ID (the one from our data table)
		var markerID = result[i].id;
		// Creating a marker and putting it on the map
		var content = result[i].extra;
		var infowindow = new google.maps.InfoWindow({
			
		});

		if (result[i]['source'] == 'jobing') {
			var marker = createMarkerRandom(latLng,map,markerTitle,google.maps.Animation.DROP,"orange",markerID);
		} else if (result[i]['source'] == 'snagajob') {
			var marker = createMarkerRandom(latLng,map,markerTitle,google.maps.Animation.DROP,"blue",markerID);
		} else if (result[i]['source']=='recruiter') {
			var marker = createMarkerRandom(latLng,map,markerTitle,google.maps.Animation.DROP,"grey",markerID);
		} else if (result[i]['source'] == 'ziprecruiter') {
			var marker = createMarkerRandom(latLng,map,markerTitle,google.maps.Animation.DROP,"green",markerID);
		} else if (result[i]['source'] == 'jobright') {
			var marker = createMarkerRandom(latLng,map,markerTitle,google.maps.Animation.DROP,"red",markerID);
		} else if (result[i]['source'] == 'careerjobsnet') {
			var marker = createMarkerRandom(latLng,map,markerTitle,google.maps.Animation.DROP,"purple",markerID);
		}  else if (result[i]['source'] == 'jobsterdotcom') {
			var marker = createMarkerRandom(latLng,map,markerTitle,google.maps.Animation.DROP,"white",markerID);
		}  else if (result[i]['source'] == 'simplyhired') {
			var marker = createMarkerRandom(latLng,map,markerTitle,google.maps.Animation.DROP,"grey",markerID);
		}  else if (result[i]['source'] == 'usjobs') {
			var marker = createMarkerRandom(latLng,map,markerTitle,google.maps.Animation.DROP,"black",markerID);
		}  else if (result[i]['source'] == 'careerbuilder') {
			var marker = createMarkerRandom(latLng,map,markerTitle,google.maps.Animation.DROP,"yellow",markerID);
		}  else if (result[i]['source'] == 'livecareer') {
			var marker = createMarkerRandom(latLng,map,markerTitle,google.maps.Animation.DROP,"grey",markerID);
		}
		
		google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){ 
			return function() {
				openBoxes.push(infowindow);
				for (xx=0;xx<openBoxes.length;xx++) {
					openBoxes[xx].close();
				}
				infowindow.setContent(content);
				infowindow.open(map,marker);
			};
		})(marker,content,infowindow));
		// push onto global array (IFF point doesn't already exist)
		already_exists = 'FALSE';
		for (xy=0;xy<allMarkers.length;xy++) {
			if (allMarkers[xy].id == marker.id) {already_exists = 'TRUE';}
		}
		if (already_exists == 'FALSE') {
			marker.setMap(map);
			allMarkers.push(marker);
		}
	}
	// cluster the markers
	var markerCluster = new MarkerClusterer(map, allMarkers,
            {imagePath: '/img/marker-cluster-image'});
	$("#progress").hide();
}

function createMarkerRandom(ll,mp,mtit,anim,icol,uniq){
	// still needs this to avoid stacks
	var min = .9999970;
	var max = 1.0001850;
	var newLat = ll.lat() * (Math.random() * (max - min) + min);
	var newLng = ll.lng() * (Math.random() * (max - min) + min);
	fnl = new google.maps.LatLng(newLat,newLng);
	var marker = new google.maps.Marker({
		position: fnl,
		title: mtit,
		animation: anim,
		icon: {
			path: google.maps.SymbolPath.CIRCLE,
			scale: 8,
			strokeColor: icol
		},
		id: uniq
	});
//	marker.setIcon(icon);	
	return marker;
}

function resizeVertical() {	
	// dynamically set map region height
	if ($(window).width() > 930) {
		var properMapHeight = $(window).height() - 44;
	} else if ($(window).width() > 703) {
		var properMapHeight = $(window).height() - 8;
	} else {
		var properMapHeight = $(window).height() - 16;
	}
	$("#main-map-region").height(properMapHeight);
	
	// dynamically set job list bar height
	var restOfScreen = $("#main-map-region").height() - 428;
	$("#right-container-element-top").height(restOfScreen);
}

function resizeHorizontal() {
	// dynamically set map region width
	if ($(window).width() > 1140) {
		var properMapWidth = $(window).width() - 225;
	} else {
		var properMapWidth = $(window).width() - 16;
	}
	$("#main-map-region").width(properMapWidth);
}

function toggleMapListView() {	
	center = map.getCenter();
	lLat = center.lat();
	lLong = center.lng();
	lZoom = map.getZoom();
	jQuery("#progress").show();
	lCat = jQuery("#cat-selector").val();
	lFilter = jQuery("#main-search-text").val();

	// toggle
	if (jQuery("#google-maps-container").is(":visible")) {
		jQuery.ajax({data:{"latitude":lLat, "longitude":lLong, "zoom":lZoom, "category":lCat, "filter":lFilter}, method:"POST",url: "/ajax/toggleView.php", success: function(result){
			jQuery("#google-maps-container").fadeOut();
			jQuery("#main-list-region").html(result).fadeIn();
			jQuery("#list-image").hide();
			jQuery("#map-image").show();
		}});			
	} else {
		jQuery("#main-list-region").fadeOut();		
		jQuery("#google-maps-container").fadeIn();
		jQuery("#map-image").hide();
		jQuery("#list-image").show();
	}
	jQuery("#progress").fadeOut();
}

// resize elements on window resize
$(window).resize(function() {
	resizeVertical();
	resizeHorizontal();
});

// anonymous function - page load
jQuery(function () {
	//correct sizing
	resizeVertical();
	resizeHorizontal();
	
	// explode the URL to set city & category
	var lCity = null;
	var lCat = null;
	var urlexplode = window.location.href;
	var i = urlexplode.indexOf("#");
	if (i > 0) {
		var urlarray = urlexplode.split("#");
		if (urlarray !== null) {
			var fullpath = urlarray[1];
			var pathArray = fullpath.split("-");
			lCity = pathArray[0];
			lCat = pathArray[1];
		}
	} else if (navigator.geolocation) {
		// run navigator geolocate & ajax to find nearest available city w/ data (so long as location exists)			
		navigator.geolocation.getCurrentPosition(setPosition);
		function setPosition (position) {
			jQuery.ajax({url:"/ajax/findClosestCity.php",data:{lat:position.coords.latitude, lng:position.coords.longitude}, success: function(result){
			console.log(result);
				lCity = result;
				lCat = "acc";
				initialize(lCat, lCity, null);
			}});
		}
	}

	// call ajax to populate the category selector box from the database
	jQuery.ajax({url: "/ajax/loadJobCats.php", success: function(result){
		jQuery('#cat-selector').append(result);
	}});
	
	// call ajax to populate the city selector box from the database
	jQuery.ajax({url: "/ajax/loadJobCities.php", success: function(result){
		jQuery('#city-selector').append(result);
	}});

	initialize(lCat, lCity, null);
		/**setInterval(function() {
		jQuery.ajax({url: "/ajax/getJobCount.php", success: function(result){
			jQuery('#total_job_count').text(result.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " Jobs Listed");
		}});		
	}, 5 * 1000); // 5 * 1000 milsec	*/
});

jQuery(document).ready(function() {
	jQuery( "#progress" ).progressbar({
		value: false
	});
	
	// toogle map list view event
	jQuery(".right-menu-item").bind("click",function() {
		toggleMapListView();
	});
	
	// change the job title
	jQuery("#cat-selector").change(function () {
		if (jQuery("#city-selector").val() == "Choose City") {alert("Choose City");return false;}
		if (jQuery("#main-list-region").is(":visible")) {
			jQuery("#main-list-region").fadeOut();		
			jQuery("#google-maps-container").fadeIn();
		}
		initialize(jQuery("#cat-selector").val(), jQuery("#city-selector").val());
	});

	// change the job title
	jQuery("#city-selector").change(function () {
        if (jQuery("#cat-selector").val() == "Choose Category") {alert("Choose Category");return false;}
		if (jQuery("#main-list-region").is(":visible")) {
			jQuery("#main-list-region").fadeOut();		
			jQuery("#google-maps-container").fadeIn();	
		}				
				
		initialize(jQuery("#cat-selector").val(), jQuery("#city-selector").val());
	});

	// the search box change
	// change the job title
	jQuery("#main-search").click(function () {
		if (jQuery("#main-list-region").is(":visible")) {
			jQuery("#main-list-region").fadeOut();		
			jQuery("#google-maps-container").fadeIn();
		}
		initialize(jQuery("#cat-selector").val(), jQuery("#city-selector").val(),jQuery("#main-search-text").val());
	});		
});