<!DOCTYPE HTML>
<!-- author : Jeet Mahanti -->
<!-- date : 20150715 --> 	
<html lang="en">
<head>
<script>
    (function(ac) {
      var d = document, s = 'script', id = 'adplugg-adjs';
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = '//www.adplugg.com/apusers/serve/' + ac + '/js/1.1/ad.js';
      fjs.parentNode.insertBefore(js, fjs);
    }('A48210703'));
</script>

<title>Find Your Location | Find Your Job</title>
<link rel="stylesheet" type="text/css" href="/css/main.css" />
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBlCPR3HoJe-TswHqUih9SZEowBcbp4gQM"></script>
<script src="/js/markerclusterer.js">
</script>
<script type="text/javascript" src="/js/main.js"></script>
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
</head>

<body>
<div id="main">
	<!-- BEGIN HEADER --> 
	<div id="header">
		<div id="selector-container">
			<div id="widget-container">
				<div id="city-selector-container">
					<select class="styled-select slate" id="city-selector"></select>
				</div>
				<div id="cat-selector-container">
					<select class="styled-select slate" id="cat-selector"></select>
				</div>
				<div id="search-container">
					<input id="main-search-text" name="main-search-text-name" type="text" placeholder="    -- SEARCH TERM --" size="20" />
					<img id="main-search" src="/img/search-thumb.png" width="20" height="20"/>
				</div>
				<div id='progress'></div>
			</div>
			<div id="right-container">
				<span class='right-menu-item' id='legend-image'><img src="/img/key-thumb.png" width="20" height="20"></img></span>		
				<span class='right-menu-item' id='list-image'><img src="/img/list-view-thumb.png" width="20" height="20"></img></span>
				<span class='right-menu-item' id='map-image'><img src="/img/map-view-thumb.png" width="20" height="20"></img></span>
			</div>

		</div>		
	</div>
    <!-- END HEADER -->
	<div id="main-container" style="position:relative">
		<!-- left container boxes -->
		<div class="container-main-sub" style="display:none">
			<div class="container-element right"> <!-- ad server box top left -->
			</div>
		</div>
		<div id="main-map-region">
			<div id="google-maps-container"></div>
			<div id="main-list-region"></div>
			<div id="copyright-container">
				<div id="copyright-text"><strong style="color">Marker locations may not be exact</strong>&nbsp&nbsp&copy;2017 MapMyJobs LLC, Denver, Colorado</div>
			</div>
		</div>
		<!-- right container boxes -->
		<div class="container-main-sub">
			<div class="container-element" id="right-container-element-top">
				<div class="container-heading">JOB LIST</div>
			</div>
			<div class="container-element" id="right-container-element-middle">
				<div class="adplugg-tag" data-adplugg-zone="top_ad_zone"></div>
			</div>
			<div class="container-element" id="right-container-element-bottom">
				<div class="adplugg-tag" data-adplugg-zone="bottom_ad_zone"></div>
			</div>
		</div>
	</div>
</div>
<!-- Google Analytics -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-80878032-1', 'auto');
ga('send', 'pageview');
</script>
</body>
</html>
